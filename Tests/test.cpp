#include "pch.h"
#include "../wordSortingProject/WordSorting.h"

class WordSortingTest : public testing::Test
{
protected:
    const std::vector<std::string> wordsToSort{ "I", "never", "the", "book", "thought", "that", "the", "so", "first", "the", "edition", "book", "so", "would", "sell", "so", "long" };
    const std::vector<std::string> ascSortedWords{ "I", "book", "book", "edition", "first", "long", "never", "sell", "so", "so", "so", "that", "the", "the", "the", "thought", "would" };
    const std::vector<std::string> desSortedWords{ "would", "thought", "the", "the", "the", "that", "so", "so", "so", "sell", "never", "long", "first", "edition", "book", "book", "I" };
    const std::vector<std::string> ascNoDuplicatedSortedWords{ "I", "book", "edition", "first", "long", "never", "sell", "so", "that", "the", "thought", "would" };
    const std::vector<std::string> desNoDuplicatedSortedWords{ "would", "thought", "the", "that", "so", "sell", "never", "long", "first", "edition", "book", "I" };
};
TEST_F(WordSortingTest, DescendingNoDuplicate)
{
    WordSorting wSorting(wordsToSort, 0, 0);
    const auto& sortedWords = wSorting.SortTheWords();
    ASSERT_EQ(sortedWords.size(), desNoDuplicatedSortedWords.size());
    EXPECT_EQ(sortedWords, desNoDuplicatedSortedWords); 
}

TEST_F(WordSortingTest, AscendingNoDuplicate)
{
    WordSorting wSorting(wordsToSort, 1, 0);
    const auto& sortedWords = wSorting.SortTheWords();
    ASSERT_EQ(sortedWords.size(), ascNoDuplicatedSortedWords.size());
    EXPECT_EQ(sortedWords, ascNoDuplicatedSortedWords);
}

TEST_F(WordSortingTest, AscendingDuplicate)
{
    WordSorting wSorting(wordsToSort, 1, 1);
    const auto& sortedWords = wSorting.SortTheWords();
    ASSERT_EQ(sortedWords.size(), ascSortedWords.size());
    EXPECT_EQ(sortedWords, ascSortedWords);
}

TEST_F(WordSortingTest, DescendingDuplicate)
{
    WordSorting wSorting(wordsToSort, 0, 1);
    const auto& sortedWords = wSorting.SortTheWords();
    ASSERT_EQ(sortedWords.size(), desSortedWords.size());
    EXPECT_EQ(sortedWords, desSortedWords);
}
