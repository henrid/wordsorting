## Name
wordSortingProject.exe (see Release directory)

## Description
This app sorts the words contained in an input file. 
It can be launched via the command line with 4 arguments, the first 2 arguments are mandatory, the last 2 are optional: 
1. Name of the input file. Words in the input file may be written on many lines.  "in.txt" file is given as an example of input file in the release directory.
2. Name of the output file. All the words in the output file are written on a single line. 
3. (optional) sort order. Value 0: descending; value 1: ascending. Default value is 1. 
4. (optional) Value 0: duplicated values are not allowed (every value has only 1 occurrence). Value 1: duplicated values are allowed. Default value is 1. 

example : "wordSortingProject.exe in.txt out.txt 0 1" 
With this command line the app reads the words from the in.txt file, sort them in a descending order, allow duplicated words, and write the result in the out.txt file. 

Unit tests are provided for 4 cases, Google Test framework is needed. 

The project was developped in Visual Studio 2019.
Compilation requires C++ 2017 version. 