#include "WordSorting.h"

#include <algorithm>

WordSorting::WordSorting(const std::vector<std::string>& words, bool ascendingOrder, bool allowDuplicate) 
    : mWords(words), mAscendingOrder{ ascendingOrder }, mAllowDuplicate{ allowDuplicate }
{
}

const std::vector<std::string>&  WordSorting::SortTheWords()
{

    // sort in ascending/descending order
    if (mAscendingOrder)
    {
        std::sort(mWords.begin(), mWords.end());
    }
    else
    {
        std::sort(mWords.begin(), mWords.end(), std::greater{});
    }

    // remove duplicates
    if (!mAllowDuplicate)
    {
        mWords.erase(std::unique(mWords.begin(), mWords.end()), mWords.end());
    }
    
    return mWords;
}