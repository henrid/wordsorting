
#include "WordSorting.h"

#include <fstream>
#include <sstream>
#include <vector>

int main(int argc, char* argv[])
{
    // Check the input data validity
    if (argc < 3)
    {
        std::cerr << "Missing arguments." << std::endl;
        return 1;
    }

    std::ifstream ifs(argv[1], std::ifstream::in);
    if (!ifs)
    {
        std::cerr << "Can not open input file: " << argv[1] << std::endl;
        return 1;
    }

    std::ofstream ofs(argv[2], std::ifstream::out);
    if (!ofs)
    {
        std::cerr << "Can not open output file: " << argv[2] << std::endl;
        return 1;
    }

    // Retrieve optional input data
    bool ascendingOrder{ true };
    bool allowDuplicate{ true };
    switch (argc)
    {
    case 5:
    {
        std::istringstream(argv[4]) >> allowDuplicate;
        [[fallthrough]];
    }
    case 4:
    {
        std::istringstream(argv[3]) >> ascendingOrder;
        break;
    }
    default: 
    {
        break; 
    }
    }

    // Retrieve the input words
    std::vector<std::string> wordsFromFile;
    while (ifs)
    {
        std::string word;
        ifs >> word;
        wordsFromFile.push_back(word);
    }

    // Process the word sorting
    WordSorting wSorting(wordsFromFile, ascendingOrder, allowDuplicate);
    const auto words = wSorting.SortTheWords();

    //write to the output file 
    for (const auto& word : words)
    {
        ofs << word << " ";
    }

    return 0;
}