#pragma once

#include <iostream>
#include <vector>

class WordSorting
{
public: 
	WordSorting(const std::vector<std::string>& words, bool ascendingOrder = true, bool allowDuplicate = true);
	const std::vector<std::string>& SortTheWords(); // do the word sorting according the optional parameters
private:
	std::vector<std::string> mWords;
	bool mAscendingOrder{ true };
	bool mAllowDuplicate{ true };
};

